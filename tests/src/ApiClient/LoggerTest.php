<?php

namespace TrekkConnect\Sdk\Tests\ApiClient;

use Monolog\Handler\TestHandler;
use PHPUnit\Framework\TestCase;
use Psr\Log\LogLevel;
use TrekkConnect\Sdk\ApiClient\Logger;
use TrekkConnect\Sdk\ApiClient\RequestError;

final class LoggerTest extends TestCase
{
    /** @var TestHandler */
    private $recorder;

    /** @var Logger */
    private $logger;

    public function setUp()
    {
        $this->recorder = new TestHandler();

        $this->logger = new Logger(
            new \Monolog\Logger('test', [$this->recorder])
        );
    }

    /** @test */
    public function i_get_all_errors_by_default()
    {
        $this->logRequestErrors();

        $this->assertTrue($this->recorder->hasErrorThatContains('Invalid partner'));
        $this->assertTrue($this->recorder->hasErrorThatContains('Supplier not found'));
        $this->assertTrue($this->recorder->hasErrorThatContains('Internal server error'));
    }

    /** @test */
    public function i_can_change_the_default_error_level()
    {
        $this->logger->setDefaultErrorLogLevel(LogLevel::WARNING);
        $this->logRequestErrors();

        $this->assertTrue($this->recorder->hasWarningThatContains('Invalid partner'));
        $this->assertFalse($this->recorder->hasErrorThatContains('Invalid partner'));
        $this->assertTrue($this->recorder->hasWarningThatContains('Supplier not found'));
        $this->assertTrue($this->recorder->hasWarningThatContains('Internal server error'));
    }

    /** @test */
    public function i_can_change_the_default_error_level_and_define_error_specific_log_levels()
    {
        $this->logger->setDefaultErrorLogLevel(LogLevel::ALERT);
        $this->logger->setErrorSpecificLogLevel(1103, LogLevel::INFO);
        $this->logger->setErrorSpecificLogLevel(1006, LogLevel::DEBUG);
        $this->logRequestErrors();

        $this->assertTrue($this->recorder->hasInfoThatContains('Invalid partner'));
        $this->assertFalse($this->recorder->hasErrorThatContains('Invalid partner'));
        $this->assertFalse($this->recorder->hasAlertThatContains('Invalid partner'));
        $this->assertTrue($this->recorder->hasDebugThatContains('Supplier not found'));
        $this->assertTrue($this->recorder->hasAlertThatContains('Internal server error'));
    }

    private function logRequestErrors()
    {
        $this->logger->logRequestError(RequestError::create('Invalid partner', 1103, 200, []));
        $this->logger->logRequestError(RequestError::create('Supplier not found', 1006, 200, []));
        $this->logger->logRequestError(RequestError::create('Internal server error', 1199, 200, []));
    }
}
