<?php

namespace TrekkConnect\Sdk\Tests\ApiClient;

use PHPUnit\Framework\TestCase;
use TrekkConnect\Sdk\ApiClient\Client;
use TrekkConnect\Sdk\ApiClient\Credentials;
use TrekkConnect\Sdk\ApiClient\Http\Client as HttpClient;
use TrekkConnect\Sdk\ApiClient\Methods\Booking\BookingItem;
use TrekkConnect\Sdk\ApiClient\Methods\Booking\CreateBookingRequest;
use TrekkConnect\Sdk\ApiClient\Methods\Booking\TicketCategory;

final class ClientTest extends TestCase
{
    /** @var HttpClient */
    private $httpClient;

    /** @var Client */
    private $client;

    public function setUp()
    {
        $this->httpClient = new TestHttpClient();
        $this->client = new Client(new Credentials('', ''), null, $this->httpClient);
    }

    /** @test */
    public function i_can_make_successful_requests()
    {
        $response = $this->client->supplier()->create([
            'name'=> 'Amazing Demo Activities',
            'partnerSupplierId' => '15873',
            'partner' => '049a72c3-7a8d-48aa-94d1-0ba5a8e9e9f2',
            'contact' => [
                'name' => 'John Doe',
                'email' => 'another@example.com'
            ]
        ]);

        $this->assertSame($response->getValue('jsonrpc'), '2.0');
        $this->assertSame($response->getValue('result.supplierId'), 'b0780dc7-d605-495f-9c89-4c4f10ab4170');
        $this->assertSame($response->getValue('id'), 1);
    }

    /** @test */
    public function i_can_make_successful_booking_create_request()
    {
        $bookingRequest = new CreateBookingRequest('sup_1', 300);
        $bookingItem = new BookingItem('act_1', 'opt_1', '2018-11-16');
        $bookingItem->addTicketCategory(new TicketCategory('tic_1', 5));
        $bookingRequest->addBookingItem($bookingItem);
        $response = $this->client->booking()->create($bookingRequest);
        $this->assertSame($response->getValue('jsonrpc'), '2.0');
        $this->assertSame($response->getValue('result.bookingId'), 'boo_b0780dc7-d605-495f-9c89-4c4f10ab4170');
        $this->assertSame($response->getValue('result.expiresAt'), 11234234);
        $this->assertSame($response->getValue('id'), 1);
    }
}
