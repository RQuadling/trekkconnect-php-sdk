<?php

namespace TrekkConnect\Sdk\Tests\ApiClient\Methods\Availability;

use Assert\InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use TrekkConnect\Sdk\ApiClient\Methods\Availability\Query;

final class QueryTest extends TestCase
{
    /** @test */
    public function i_can_create_query()
    {
        $query = new Query('sup_1', 'act_2', '2018-11-15', '2019-11-15', ['opt_1', 'opt_2'], 'cur_2asfe');
        $this->assertEquals([
            'supplierId' => 'sup_1',
            'activityId' => 'act_2',
            'fromDate' => '2018-11-15',
            'untilDate' => '2019-11-15',
            'optionIds' => ['opt_1', 'opt_2'],
            'cursor' => 'cur_2asfe'
        ], $query->generate());
    }

    /** @test */
    public function i_can_create_query_with_cursor()
    {
        $query = new Query('sup_1', 'act_2', '2018-11-15', '2019-11-15', ['opt_1', 'opt_2']);
        $query = $query->withCursor('cur_2asfe');
        $this->assertEquals([
            'supplierId' => 'sup_1',
            'activityId' => 'act_2',
            'fromDate' => '2018-11-15',
            'untilDate' => '2019-11-15',
            'optionIds' => ['opt_1', 'opt_2'],
            'cursor' => 'cur_2asfe'
        ], $query->generate());
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function i_cannot_create_query_with_empty_supplier_id()
    {
        new Query('', 'act_2', '2018-11-15', '2019-11-15', ['opt_1', 'opt_2']);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function i_cannot_create_query_with_empty_activity_id()
    {
        new Query('sup_1', '', '2018-11-15', '2019-11-15', ['opt_1', 'opt_2']);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function i_cannot_create_query_with_empty_from_date()
    {
        new Query('sup_1', 'act_1', '', '2019-11-15', ['opt_1', 'opt_2']);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function i_cannot_create_query_with_incorrect_from_date()
    {
        new Query('sup_1', 'act_1', 'asdfdf', '2019-11-15', ['opt_1', 'opt_2']);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function i_cannot_create_query_with_empty_until_date()
    {
        new Query('sup_1', 'act_1', '2018-11-15', '', ['opt_1', 'opt_2']);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     */
    public function i_cannot_create_query_with_incorrect_until_date()
    {
        new Query('sup_1', 'act_1', '2018-11-15', 'ssdfsdf', ['opt_1', 'opt_2']);
    }
}
