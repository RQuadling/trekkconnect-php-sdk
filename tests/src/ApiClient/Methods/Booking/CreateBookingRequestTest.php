<?php

namespace TrekkConnect\Sdk\Tests\ApiClient\Methods\Booking;

use Assert\InvalidArgumentException;
use DateTime;
use PHPUnit\Framework\TestCase;
use TrekkConnect\Sdk\ApiClient\Methods\Booking\BookingItem;
use TrekkConnect\Sdk\ApiClient\Methods\Booking\CreateBookingRequest;
use TrekkConnect\Sdk\ApiClient\Methods\Booking\TicketCategory;

final class CreateBookingRequestTest extends TestCase
{
    /** @test */
    public function i_can_create_booking_request()
    {
        $bookingRequest = new CreateBookingRequest('sup_1', 300);
        $bookingItem = new BookingItem('act_1', 'opt_1', '2018-11-16');
        $bookingItem->addTicketCategory(new TicketCategory('tic_1', 5));
        $bookingRequest->addBookingItem($bookingItem);
        $this->assertEquals([
            'supplierId' => 'sup_1',
            'bookingItems' => [
                [
                    'activityId' => 'act_1',
                    'optionId' => 'opt_1',
                    'date' => '2018-11-16',
                    'ticketCategories' => [
                        [
                            'ticketCategory' => 'tic_1',
                            'count' => 5,
                            'externalTicketId' => ''
                        ]
                    ],
                ]
            ],
            'holdDurationSeconds' => 300
        ], $bookingRequest->generate());
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Booking Items cannot be empty
     */
    public function i_cannot_send_booking_request_without_booking_items_request()
    {
        $bookingRequest = new CreateBookingRequest('sup_1', 300);
        $bookingRequest->generate();
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Ticket categories cannot be empty
     */
    public function i_cannot_send_booking_request_without_ticket_categories_request()
    {
        $bookingRequest = new CreateBookingRequest('sup_1', 300);
        $bookingItem = new BookingItem('act_1', 'opt_1', '2018-11-16');
        $bookingRequest->addBookingItem($bookingItem);
        $bookingRequest->generate();
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Incorrect Ticket Category
     */
    public function i_cannot_send_booking_request_with_incorrect_ticket_categories()
    {
        $bookingRequest = new CreateBookingRequest('sup_1', 300);
        $bookingItem = new BookingItem('act_1', 'opt_1', '2018-11-16', [new DateTime()]);
        $bookingRequest->addBookingItem($bookingItem);
        $bookingRequest->generate();
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Incorrect Booking Item
     */
    public function i_cannot_send_booking_request_with_incorrect_booking_items()
    {
        $bookingRequest = new CreateBookingRequest('sup_1', 300, [new DateTime()]);
        $bookingRequest->generate();
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Incorrect date format
     */
    public function i_cannot_send_booking_request_with_incorrect_date()
    {
        $bookingRequest = new CreateBookingRequest('sup_1', 300);
        $bookingItem = new BookingItem('act_1', 'opt_1', 'incorrect');
        $bookingRequest->addBookingItem($bookingItem);
        $bookingItem->addTicketCategory(new TicketCategory('tic_1', 5));
        $bookingRequest->addBookingItem($bookingItem);
        $bookingRequest->generate();
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Supplier Id cannot be empty
     */
    public function i_cannot_send_booking_request_with_empty_supplier_id()
    {
        $bookingRequest = new CreateBookingRequest('', 300);
        $bookingItem = new BookingItem('act_1', 'opt_1', '2018-11-15');
        $bookingRequest->addBookingItem($bookingItem);
        $bookingItem->addTicketCategory(new TicketCategory('tic_1', 5));
        $bookingRequest->addBookingItem($bookingItem);
        $bookingRequest->generate();
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Activity Id cannot be empty
     */
    public function i_cannot_send_booking_request_with_empty_activity_id()
    {
        $bookingRequest = new CreateBookingRequest('sup_1', 300);
        $bookingItem = new BookingItem('', 'opt_1', '2018-11-15');
        $bookingRequest->addBookingItem($bookingItem);
        $bookingItem->addTicketCategory(new TicketCategory('tic_1', 5));
        $bookingRequest->addBookingItem($bookingItem);
        $bookingRequest->generate();
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Option Id cannot be empty
     */
    public function i_cannot_send_booking_request_with_empty_option_id()
    {
        $bookingRequest = new CreateBookingRequest('sup_1', 300);
        $bookingItem = new BookingItem('act_1', '', '2018-11-15');
        $bookingRequest->addBookingItem($bookingItem);
        $bookingItem->addTicketCategory(new TicketCategory('tic_1', 5));
        $bookingRequest->addBookingItem($bookingItem);
        $bookingRequest->generate();
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Ticket Category cannot be empty
     */
    public function i_cannot_send_booking_request_with_empty_ticket_category_id()
    {
        $bookingRequest = new CreateBookingRequest('sup_1', 300);
        $bookingItem = new BookingItem('act_1', 'opt_1', '2018-11-15');
        $bookingRequest->addBookingItem($bookingItem);
        $bookingItem->addTicketCategory(new TicketCategory('', 5));
        $bookingRequest->addBookingItem($bookingItem);
        $bookingRequest->generate();
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Count cannot be less than 1
     */
    public function i_cannot_send_booking_request_with_empty_incorrect_occupancy()
    {
        $bookingRequest = new CreateBookingRequest('sup_1', 300);
        $bookingItem = new BookingItem('act_1', 'opt_1', '2018-11-15');
        $bookingRequest->addBookingItem($bookingItem);
        $bookingItem->addTicketCategory(new TicketCategory('tic_1', -1));
        $bookingRequest->addBookingItem($bookingItem);
        $bookingRequest->generate();
    }
}
