<?php

namespace TrekkConnect\Sdk\ApiClient\Exceptions;

class RuntimeException extends Exception
{
}
