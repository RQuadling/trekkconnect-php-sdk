<?php

namespace TrekkConnect\Sdk\ApiClient\Exceptions;

class InvalidArgumentException extends Exception
{
}
