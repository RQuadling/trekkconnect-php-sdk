<?php

namespace TrekkConnect\Sdk\ApiClient\Http;

use GuzzleHttp\Client as GuzzleClient;
use Psr\Http\Message\RequestInterface as Request;

final class Guzzle6Client implements Client
{
    /** @var Client */
    private $client;

    /**
     * @return GuzzleClient
     */
    private function getClient()
    {
        if (!$this->client) {
            $this->client = new GuzzleClient([
                'timeout' => 300,
                'verify' => false,
                'http_errors' => false,
            ]);
        }

        return $this->client;
    }

    /**
     * {@inheritdoc}
     */
    public function request(Request $request)
    {
        return $this->getClient()->send($request);
    }
}
