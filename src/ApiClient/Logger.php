<?php

namespace TrekkConnect\Sdk\ApiClient;

use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class Logger extends AbstractLogger
{
    /** @var LoggerInterface */
    private $logger;

    /** @var string */
    private $defaultErrorLogLevel = LogLevel::ERROR;

    /** @var string[] */
    private $logLevelsByErrorCode = [];

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $logLevel
     */
    public function setDefaultErrorLogLevel($logLevel)
    {
        $this->defaultErrorLogLevel = $logLevel;
    }

    /**
     * @param int    $errorCode
     * @param string $logLevel
     */
    public function setErrorSpecificLogLevel($errorCode, $logLevel)
    {
        $this->logLevelsByErrorCode[$errorCode] = $logLevel;
    }

    public function logRequestError(RequestError $e)
    {
        $logLevel = array_key_exists($e->getCode(), $this->logLevelsByErrorCode)
            ? $this->logLevelsByErrorCode[$e->getCode()]
            : $this->defaultErrorLogLevel;

        $this->log($logLevel, "Request failed: {$e->getMessage()} (E{$e->getCode()})", [
            'message' => $e->getMessage(),
            'code' => $e->getCode(),
            'details' => $e->getDetails(),
        ]);
    }

    public function log($level, $message, array $context = [])
    {
        $this->logger->log($level, $message, $context);
    }
}
