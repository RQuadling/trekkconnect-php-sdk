<?php

namespace TrekkConnect\Sdk\ApiClient;

use TrekkConnect\Sdk\ApiClient\Exceptions\RuntimeException;

class RequestError extends RuntimeException
{
    /** @var int */
    private $statusCode = 0;

    /** @var array */
    private $details = [];

    /** @var array */
    private $debugInfo = [];

    /**
     * @param string   $errorMessage
     * @param int      $errorCode
     * @param int      $statusCode
     * @param string[] $details
     * @param string[] $debugInfo
     *
     * @return self
     */
    public static function create($errorMessage, $errorCode, $statusCode, array $details, array $debugInfo = [])
    {
        $e = new static($errorMessage, $errorCode);
        $e->statusCode = $statusCode;
        $e->details = $details;
        $e->debugInfo = $debugInfo;

        return $e;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @return array
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @return array
     */
    public function getDebugInfo()
    {
        return $this->debugInfo;
    }
}
