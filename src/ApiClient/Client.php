<?php

namespace TrekkConnect\Sdk\ApiClient;

use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\Cache;
use GuzzleHttp\Psr7\Uri;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use TrekkConnect\Sdk\ApiClient\Http\Client as HttpClient;
use TrekkConnect\Sdk\ApiClient\Http\Guzzle6Client;
use TrekkConnect\Sdk\ApiClient\Http\Request;
use TrekkConnect\Sdk\ApiClient\Http\Response;
use TrekkConnect\Sdk\ApiClient\Methods\Activity;
use TrekkConnect\Sdk\ApiClient\Methods\Availability;
use TrekkConnect\Sdk\ApiClient\Methods\Booking;
use TrekkConnect\Sdk\ApiClient\Methods\Mapping;
use TrekkConnect\Sdk\ApiClient\Methods\Supplier;
use TrekkConnect\Sdk\ApiClient\Methods\Ticket;

class Client
{
    /** @var string */
    private $baseUrl = 'https://api.trekkconnect.com';

    /** @var Credentials */
    private $credentials;

    /** @var HttpClient */
    private $httpClient;

    /** @var Logger */
    private $logger;

    /** @var Cache */
    private $cache;

    /** @var bool */
    private $useCache = false;

    /** @var int */
    private $cacheTtl = 30;

    /** @var string */
    private $correlationId = null;

    /**
     * @param Credentials          $credentials
     * @param Cache                $cache
     * @param LoggerInterface|null $logger
     * @param HttpClient|null      $httpClient
     *
     * @return self
     */
    public static function newCachingClient(
        Credentials $credentials,
        Cache $cache,
        LoggerInterface $logger = null,
        HttpClient $httpClient = null
    ) {
        return (new static($credentials, $logger, $httpClient))->withCache(false, $cache);
    }

    /**
     * @param Credentials          $credentials
     * @param LoggerInterface|null $logger
     * @param HttpClient|null      $httpClient
     */
    public function __construct(Credentials $credentials, LoggerInterface $logger = null, HttpClient $httpClient = null)
    {
        $this->credentials = $credentials;
        $this->logger = $logger instanceof Logger ? $logger : new Logger($logger ?: new NullLogger());
        $this->httpClient = $httpClient ?: new Guzzle6Client();
        $this->cache = new ArrayCache();
    }

    /**
     * We don't clone the cache as it may have been referenced from the outside to gather information.
     */
    public function __clone()
    {
        $this->credentials = clone $this->credentials;
        $this->httpClient = clone $this->httpClient;
        $this->logger = clone $this->logger;
    }

    /**
     * @param Credentials $credentials
     *
     * @return $this
     */
    public function withNewCredentials(Credentials $credentials)
    {
        $clone = clone $this;
        $clone->credentials = $credentials;

        return $clone;
    }

    /**
     * @param string $baseUrl
     *
     * @return $this
     */
    public function withOtherBaseUrl($baseUrl)
    {
        $clone = clone $this;
        $clone->baseUrl = $baseUrl;

        return $clone;
    }

    /**
     * @param string $correlationId
     *
     * @return $this
     */
    public function withCorrelationId($correlationId)
    {
        $clone = clone $this;
        $clone->correlationId = $correlationId;

        return $clone;
    }

    /**
     * @return Cache
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * @param false|null|int $ttl
     * @param Cache|null     $cache
     *
     * @return $this
     */
    public function withCache($ttl = null, Cache $cache = null)
    {
        $clone = clone $this;
        if ($cache) {
            $clone->cache = $cache;
        }

        if ($ttl === false) {
            $clone->useCache = false;
        } else {
            $clone->useCache = true;

            if ($ttl !== null) {
                $clone->cacheTtl = $ttl;
            }
        }

        return $clone;
    }

    /**
     * @return Supplier
     */
    public function supplier()
    {
        return new Supplier($this);
    }

    /**
     * @return Activity
     */
    public function activity()
    {
        return new Activity($this);
    }

    /**
     * @return Availability
     */
    public function availability()
    {
        return new Availability($this);
    }

    /**
     * @return Booking
     */
    public function booking()
    {
        return new Booking($this);
    }

    /**
     * @return Ticket
     */
    public function ticket()
    {
        return new Ticket($this);
    }

    /**
     * @return Mapping
     */
    public function mapping()
    {
        return new Mapping($this);
    }

    /**
     * @param string $method
     * @param array $params
     *
     * @return Response
     * @throws RequestError
     */
    public function request($method, array $params = [])
    {
        $data = \GuzzleHttp\json_encode([
            'jsonrpc' => '2.0',
            'method' => $method,
            'params' => new \ArrayObject($params),
            'id' => 1,
        ]);

        $cacheKey = md5($this->credentials->getPublicKey().':'.$data);

        if ($this->useCache && $response = $this->cache->fetch($cacheKey)) {
            return $response;
        }

        $request = new Request('POST', '/v1');
        $request->getBody()->write($data);
        $request->getBody()->rewind();

        $response = $this->doRequest($request);

        if ($this->useCache) {
            $response->setCachedSince(new \DateTimeImmutable());
            $this->cache->save($cacheKey, $response, $this->cacheTtl);
        }

        return $response;
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @throws Exceptions\OutOfBoundsException
     * @throws Exceptions\RuntimeException
     * @throws Http\ConnectionError
     * @throws RequestError
     */
    private function doRequest(Request $request)
    {
        $request->setLogger($this->logger);

        $uri = new Uri($this->baseUrl.$request->getRequestTarget());

        $this->logger->info("Request: {$request->getMethod()} {$uri}");

        /** @var Request $request */
        $request = $request->withAuthorizationHeader($this->credentials);
        $request = $request->withUri($uri);
        $request = $request->withMethod('post');
        $request = $request->withHeader('Accept', 'application/json');
        $request = $request->withHeader('Content-Type', 'application/json');

        if ($this->correlationId !== null) {
            $request = $request->withHeader('X-Correlation-ID', $this->correlationId);
        }

        $response = Response::fromPsr7($this->httpClient->request($request));

        try {
            $response->assertSuccessful();
        } catch (RequestError $e) {
            $this->logger->logRequestError($e);
            throw $e;
        }

        return $response;
    }
}
