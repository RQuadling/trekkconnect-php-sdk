<?php

namespace TrekkConnect\Sdk\ApiClient\Methods\Activity;

use Assert\Assert;

class Query
{
    /**
     * @var string
     */
    private $supplierId;
    /**
     * @var array
     */
    private $activityIds = [];
    /**
     * @var string|null
     */
    private $cursor;


    /**
     * @param $supplierId
     * @param array $activityIds
     * @param string|null $cursor
     */
    public function __construct($supplierId, array $activityIds = [], $cursor = null)
    {
        Assert::that($supplierId)->notEmpty($supplierId, 'Supplier Id cannot be empty');
        $this->supplierId = (string)$supplierId;
        $this->activityIds = $activityIds;
        $this->cursor = $cursor;
    }

    /**
     * @param $cursor
     * @return Query
     */
    public function withCursor($cursor)
    {
        $query = clone $this;
        $query->cursor = $cursor;
        return $query;
    }

    /**
     * @return array
     */
    public function generate()
    {
        return [
            'supplierId' => (string)$this->supplierId,
            'activityIds' => (array)$this->activityIds,
            'cursor' => $this->cursor
        ];
    }
}