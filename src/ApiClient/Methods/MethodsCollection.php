<?php

namespace TrekkConnect\Sdk\ApiClient\Methods;

use TrekkConnect\Sdk\ApiClient\Client;
use TrekkConnect\Sdk\ApiClient\Http\Response;

abstract class MethodsCollection
{
    /** @var Client */
    protected $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $method
     * @param array  $params
     *
     * @return Response
     */
    protected function request($method, array $params)
    {
        return $this->client->request($method, $params);
    }
}
