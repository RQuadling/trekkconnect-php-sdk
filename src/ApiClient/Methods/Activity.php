<?php

namespace TrekkConnect\Sdk\ApiClient\Methods;

use TrekkConnect\Sdk\ApiClient\Http\Response;
use TrekkConnect\Sdk\ApiClient\Methods\Activity\Query;

final class Activity extends MethodsCollection
{

    /**
     * @param array $params
     *
     * @return Response
     */
    public function updated(array $params)
    {
        return $this->request('activity.updated', $params);
    }

    /**
     * @param Query $query
     * @return Response
     */
    public function find(Query $query)
    {
        return $this->request('activity.find', [
            'query' => $query->generate()
        ]);
    }
}
