<?php

namespace TrekkConnect\Sdk\ApiClient\Methods\Booking;

use Assert\Assert;

class BookingItem
{
    /**
     * @var
     */
    protected $activityId;
    /**
     * @var
     */
    protected $optionId;
    /**
     * @var
     */
    protected $date;
    /**
     * @var array
     */
    protected $ticketCategories;

    /**
     * @param string $activityId
     * @param string $optionId
     * @param string $date
     * @param array $ticketCategories
     */
    public function __construct($activityId, $optionId, $date, array $ticketCategories = [])
    {
        Assert::that($activityId)->notEmpty('Activity Id cannot be empty');
        Assert::that($optionId)->notEmpty('Option Id cannot be empty');
        Assert::that($date)->regex('/^\d{4}-\d{2}-\d{2}$/', 'Incorrect date format');
        $this->activityId = $activityId;
        $this->optionId = $optionId;
        $this->date = $date;
        $this->ticketCategories = $ticketCategories;
    }

    /**
     * @param TicketCategory $ticketCategory
     */
    public function addTicketCategory(TicketCategory $ticketCategory)
    {
        $this->ticketCategories[] = $ticketCategory;
    }

    /**
     * @return array
     */
    public function generate()
    {
        Assert::that($this->ticketCategories)->notEmpty('Ticket categories cannot be empty')->all()->isInstanceOf(TicketCategory::class, 'Incorrect Ticket Category');
        return [
            'activityId' => $this->activityId,
            'optionId' => $this->optionId,
            'date' => $this->date,
            'ticketCategories' => $this->getTicketCategoriesAsArray()
        ];
    }

    /**
     * @return array
     */
    protected function getTicketCategoriesAsArray()
    {
        $ticketCategories = [];

        /** @var $ticketCategory TicketCategory*/
        foreach ($this->ticketCategories as $ticketCategory) {
            $ticketCategories[] = $ticketCategory->generate();
        }
        return $ticketCategories;
    }
}