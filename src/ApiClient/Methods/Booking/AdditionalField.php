<?php

namespace TrekkConnect\Sdk\ApiClient\Methods\Booking;

use Assert\Assert;

class AdditionalField
{
    /**
     * @var
     */
    private $key;
    /**
     * @var
     */
    private $value;

    /**
     * @param string $key
     * @param string $value
     */
    public function __construct($key, $value)
    {
        Assert::that($key)->notEmpty('Additional Field Key cannot be empty');
        Assert::that($value)->notEmpty('Additional Field Value cannot be empty');
        $this->key = $key;
        $this->value = $value;
    }

    /**
     * @return array
     */
    public function generate()
    {
        return [
            'key' => $this->key,
            'value' => $this->value,
        ];
    }
}