<?php

namespace TrekkConnect\Sdk\ApiClient\Methods\Booking;

use Assert\Assert;

class Payment
{
    private $amount;
    private $currency;

    public function __construct($amount, $currency)
    {
        Assert::that($amount)->notEmpty('Amount cannot be empty');
        Assert::that($currency)->notEmpty('Currency cannot be empty');
        $this->amount = $amount;
        $this->currency = $currency;
    }

    public function generate()
    {
        return [
            'amount' => $this->amount,
            'currency' => $this->currency,
        ];
    }
}