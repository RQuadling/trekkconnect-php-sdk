<?php

namespace TrekkConnect\Sdk\ApiClient\Methods\Booking;

class Guest
{
    /**
     * @var string|null
     */
    private $firstName;
    /**
     * @var string|null
     */
    private $lastName;
    /**
     * @var string|null
     */
    private $emailAddress;
    /**
     * @var string|null
     */
    private $phoneNumber;
    /**
     * @var array
     */
    private $additionalFields = [];
    /**
     * @var array
     */
    private $addons = [];

    /**
     * @param string|null $firstName
     * @param string|null $lastName
     * @param string|null $emailAddress
     * @param string|null $phoneNumber
     * @param array $additionalFields
     * @param array $addons
     */
    public function __construct(
        $firstName = null,
        $lastName = null,
        $emailAddress = null,
        $phoneNumber = null,
        array $additionalFields = [],
        array $addons = []
    ) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->emailAddress = $emailAddress;
        $this->phoneNumber = $phoneNumber;
        $this->additionalFields = $additionalFields;
        $this->addons = $addons;
    }

    /**
     * @param Addon $addon
     */
    public function addAddon(Addon $addon)
    {
        $this->addons[] = $addon;
    }

    /**
     * @param AdditionalField $additionalField
     */
    public function addAdditionalField(AdditionalField $additionalField)
    {
        $this->additionalFields[] = $additionalField;
    }

    /**
     * @return array
     */
    public function generate()
    {
        return [
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'emailAddress' => $this->emailAddress,
            'phoneNumber' => $this->phoneNumber,
            'additionalFields' => $this->getAdditionalFieldsAsArray(),
            'addons' => $this->getAddonsAsArray(),
        ];
    }

    /**
     * @return array
     */
    private function getAddonsAsArray()
    {
        $addons = [];

        /** @var $addon Addon*/
        foreach ($this->addons as $addon) {
            $addons[] = $addon->generate();
        }
        return $addons;
    }

    /**
     * @return array
     */
    private function getAdditionalFieldsAsArray()
    {
        $additionalFields = [];

        /** @var $additionalField AdditionalField*/
        foreach ($this->additionalFields as $additionalField) {
            $additionalFields[] = $additionalField->generate();
        }
        return $additionalFields;
    }
}