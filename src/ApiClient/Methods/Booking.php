<?php

namespace TrekkConnect\Sdk\ApiClient\Methods;

use TrekkConnect\Sdk\ApiClient\Http\Response;
use TrekkConnect\Sdk\ApiClient\Methods\Booking\CancelBookingRequest;
use TrekkConnect\Sdk\ApiClient\Methods\Booking\CommitBookingRequest;
use TrekkConnect\Sdk\ApiClient\Methods\Booking\CreateBookingRequest;

final class Booking extends MethodsCollection
{

    /**
     * @param string $bookingId
     *
     * @return Response
     */
    public function cancelled($bookingId)
    {
        return $this->request('booking.cancelled', [
            'bookingId' => $bookingId
        ]);
    }

    /**
     * @param CancelBookingRequest $request
     * @return Response
     */
    public function cancel(CancelBookingRequest $request)
    {
        return $this->request('booking.cancel', $request->generate());
    }

    public function create(CreateBookingRequest $request)
    {
        return $this->request('booking.create', $request->generate());
    }

    public function commit(CommitBookingRequest $request)
    {
        return $this->request('booking.commit', $request->generate());
    }
}
