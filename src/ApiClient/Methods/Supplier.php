<?php

namespace TrekkConnect\Sdk\ApiClient\Methods;

use TrekkConnect\Sdk\ApiClient\Http\Response;

final class Supplier extends MethodsCollection
{

    /**
     * @param array $params
     *
     * @return Response
     */
    public function create(array $params)
    {
        return $this->request('supplier.create', $params);
    }


    public function enable($supplierId)
    {
        return $this->request('supplier.enable', [
            'supplierId' => $supplierId
        ]);
    }

    public function disable($supplierId)
    {
        return $this->request('supplier.disable', [
            'supplierId' => $supplierId
        ]);
    }
}
